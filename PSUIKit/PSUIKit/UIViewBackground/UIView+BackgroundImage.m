//
//  UIView+BackgroundImage.m
//  PSUIKit
//
//  Created by Pavel Smelovsky on 09.05.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

#import "UIView+BackgroundImage.h"
#import <objc/runtime.h>

#import <PSUIKit/NSWeakReference.h>
#import <PSUIKit/Swizzling.h>
#import <PSUIKit/ImageLayer.h>

static void *kBackgroundImageProperty = &kBackgroundImageProperty;
static void *kImageModeProperty = &kImageModeProperty;
static void *kBackgroundLayerProperty = &kBackgroundLayerProperty;

@interface UIView()

@property (nonatomic, weak) ImageLayer *backgroundLayer;

@end

#pragma mark - Implementation

@implementation UIView (BackgroundImage)

+ (void)load {
    [super load];

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        SwizzleMethod(self, @selector(layoutSublayersOfLayer:), @selector(custom_layoutSublayersOfLayer:));
    });
}

- (void)custom_layoutSublayersOfLayer:(CALayer *)layer {
    [self custom_layoutSublayersOfLayer:layer];

    if (self.backgroundLayer) {
        [CATransaction begin];
        [CATransaction setDisableActions:YES];

        [self.backgroundLayer setFrame:self.bounds];

        [CATransaction commit];
    }
}

- (void)updateBackgroundLayerWithImage:(UIImage *)backgroundImage {
    if (self.backgroundLayer == nil) {
        self.backgroundLayer = [self createBackgroundLayer];
    }

    self.backgroundLayer.image = backgroundImage.CGImage;
}

- (void)updateBackgroundLayerWithImageMode:(ImageMode)imageMode {
    if (self.backgroundLayer) {
        self.backgroundLayer.imageMode = imageMode;
    }
}

- (ImageLayer *)createBackgroundLayer {
    ImageLayer *layer = [ImageLayer layerWithImage:nil imageMode:self.imageMode];
    [self.layer insertSublayer:layer atIndex:0];
    return layer;
}

- (void)removeBackgroundLayer {
    [self.backgroundLayer removeFromSuperlayer];
    self.backgroundLayer = nil;
}

#pragma mark - Public properties

- (UIImage *)backgroundImage {
    return objc_getAssociatedObject(self, kBackgroundImageProperty);
}

- (void)setBackgroundImage:(UIImage *)backgroundImage {
    [self willChangeValueForKey:@"backgroundImage"];
    objc_setAssociatedObject(self, kBackgroundImageProperty, backgroundImage, OBJC_ASSOCIATION_COPY_NONATOMIC);

    // create or update backgroundLayer
    if (backgroundImage != nil) {
        [self updateBackgroundLayerWithImage:backgroundImage];
    } else {
        [self removeBackgroundLayer];
    }

    [self didChangeValueForKey:@"backgroundImage"];
}

- (ImageMode)imageMode {
    id object = objc_getAssociatedObject(self, kImageModeProperty);
    return [object unsignedIntegerValue];
}

- (void)setImageMode:(ImageMode)imageMode {
    [self willChangeValueForKey:@"imageMode"];

    id object = [NSNumber numberWithUnsignedInteger:imageMode];
    objc_setAssociatedObject(self, kImageModeProperty, object, OBJC_ASSOCIATION_COPY_NONATOMIC);

    [self updateBackgroundLayerWithImageMode:imageMode];

    [self didChangeValueForKey:@"imageMode"];
}

#pragma mark - Internal properties

- (CALayer *)backgroundLayer {
    id object = objc_getAssociatedObject(self, kBackgroundLayerProperty);
    if ([object isKindOfClass:NSWeakReference.class]) {
        return ((NSWeakReference *)object).object;
    }
    return nil;
}

- (void)setBackgroundLayer:(CALayer *)backgroundLayer {
    [self willChangeValueForKey:@"backgroundLayer"];

    if (backgroundLayer != nil) {
        id object = [NSWeakReference weakReference:backgroundLayer];
        objc_setAssociatedObject(self, kBackgroundLayerProperty, object, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    } else {
        objc_setAssociatedObject(self, kBackgroundLayerProperty, nil, OBJC_ASSOCIATION_ASSIGN);
    }

    [self didChangeValueForKey:@"backgroundLayer"];
}

@end
