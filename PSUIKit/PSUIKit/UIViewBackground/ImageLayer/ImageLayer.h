//
//  ImageLayer.h
//  PSUIKit
//
//  Created by Pavel Smelovsky on 24.04.15.
//  Copyright (c) 2015 Pavel Smelovsky. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "CGImageFunctions.h"

@interface ImageLayer : CALayer

+ (instancetype)layerWithImage:(CGImageRef)image imageMode:(ImageMode)imageMode NS_SWIFT_NAME(init(image:mode:));

@property (nonatomic, assign) CGImageRef image;
@property (nonatomic, assign) ImageMode imageMode;

@end
