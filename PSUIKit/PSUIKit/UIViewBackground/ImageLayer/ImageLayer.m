//
//  ImageLayer.m
//  PSUIKit
//
//  Created by Pavel Smelovsky on 24.04.15.
//  Copyright (c) 2015 Pavel Smelovsky. All rights reserved.
//

#import "ImageLayer.h"

#import <UIKit/UIKit.h>
#import "CGImageFunctions.h"

@implementation ImageLayer

@dynamic image;

+ (BOOL)needsDisplayForKey:(NSString *)key
{
    if ([key isEqualToString:@"image"]) {
        return YES;
    } else if ([key isEqualToString:@"imageMode"]) {
        return YES;
    }
    
    return [super needsDisplayForKey:key];
}

+ (instancetype)layerWithImage:(CGImageRef)image imageMode:(ImageMode)imageMode {
    ImageLayer *layer = [[self alloc] init];
    layer.image = image;
    layer.imageMode = imageMode;
    return layer;
}

- (instancetype)init
{
    if (self = [super init]) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.drawsAsynchronously = YES;
    self.needsDisplayOnBoundsChange = YES;
    
    self.contentsScale = [[UIScreen mainScreen] scale];
    self.imageMode = ImageModeFill;
}

- (void)drawInContext:(CGContextRef)ctx
{
#ifdef DEBUG
    NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
#endif
    
    CGContextTranslateCTM(ctx, 0, CGRectGetHeight(self.bounds));
    CGContextScaleCTM(ctx, 1.0, -1.0);

    CGRect imageRect = CGRectForImage(self.image, self.imageMode, self.bounds);
    CGContextDrawImage(ctx, imageRect, self.image);

#ifdef DEBUG
    NSLog(@"Time to draw background: %.7f", [NSDate timeIntervalSinceReferenceDate] - start);
#endif
}

@end
