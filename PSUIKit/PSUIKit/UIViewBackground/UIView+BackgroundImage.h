//
//  UIView+BackgroundImage.h
//  PSUIKit
//
//  Created by Pavel Smelovsky on 09.05.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CGImageFunctions.h"

@interface UIView (BackgroundImage)

@property (nonatomic, copy, nullable) IBInspectable UIImage *backgroundImage NS_SWIFT_NAME(backgroundImage);
@property (nonatomic, assign) IBInspectable ImageMode imageMode;

@end
