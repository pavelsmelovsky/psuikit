//
//  UIView+BacksideView.h
//  PSUIKit
//
//  Created by Pavel Smelovsky on 09.05.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (BacksideView)

@property (nonatomic, weak, nullable) IBOutlet UIView *backsideView NS_SWIFT_NAME(backsideView);

@end
