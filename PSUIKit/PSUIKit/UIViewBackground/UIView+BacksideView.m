//
//  UIView+backsideView.m
//  PSUIKit
//
//  Created by Pavel Smelovsky on 09.05.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

#import "UIView+BacksideView.h"
#import <objc/runtime.h>

#import <PSUIKit/Swizzling.h>
#import <PSUIKit/NSWeakReference.h>

static void *kBacksideViewProperty = &kBacksideViewProperty;

#pragma mark - Declarations

@interface UIView()

@end

#pragma mark - Implementation

@implementation UIView (BacksideView)

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        SwizzleMethod(self, @selector(layoutSubviews), @selector(custom_layoutSubviews));
    });
}

- (void)custom_layoutSubviews {
    [self custom_layoutSubviews];

    if (self.backsideView != nil) {
        self.backsideView.frame = self.bounds;
    }
}

- (void)updateBacksideView:(UIView *)backsideView {
    backsideView.translatesAutoresizingMaskIntoConstraints = NO;
    backsideView.frame = self.bounds;

    [self insertSubview:backsideView atIndex:0];
}

- (void)removeBacksideView:(UIView *)backsideView {
    [backsideView removeFromSuperview];
}

#pragma mark - Public properties

- (UIView *)backsideView {
    id weakReference = objc_getAssociatedObject(self, kBacksideViewProperty);
    if ([weakReference isKindOfClass:NSWeakReference.class]) {
        return ((NSWeakReference *)weakReference).object;
    }
    return nil;
}

- (void)setBacksideView:(UIView *)backsideView {
    [self willChangeValueForKey:@"backsideView"];

    if (backsideView != nil) {
        id weakRef = [NSWeakReference weakReference:backsideView];
        objc_setAssociatedObject(self, kBacksideViewProperty, weakRef, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

        [self updateBacksideView:backsideView];
    } else {
        UIView *oldBacksideView;
        if ((oldBacksideView = self.backsideView) != nil) {
            [self removeBacksideView:oldBacksideView];
        }

        objc_setAssociatedObject(self, kBacksideViewProperty, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }

    [self didChangeValueForKey:@"backsideView"];
}

@end
