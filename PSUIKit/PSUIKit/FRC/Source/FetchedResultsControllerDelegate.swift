//
//  FetchedResultsControllerDelegate.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 18.04.16.
//  Copyright © 2016 Pavel Smelovsky. All rights reserved.
//

import Foundation
import CoreData

public protocol FetchedResultsControllerDelegate: class {
    /**
     Update fetched results controller and return it
     
     - parameter frc: Update this controller, or create new and return it
     
     - returns: Updated or newly created NSFetchedResultsController instance
     */
    func fetchedResultsController<RequestResult: NSFetchRequestResult>(_ controller: FetchedResultsController<RequestResult>, update fetchedResultsController: NSFetchedResultsController<RequestResult>?) -> NSFetchedResultsController<RequestResult>?

    /**
     Perform updating data from webserver or other destination
     
     - parameter completion: Call this completion block when data loaded
     */
    func fetchedResultsController<RequestResult: NSFetchRequestResult>(_ controller: FetchedResultsController<RequestResult>, loadWithCompletion completion: @escaping () -> Void)

    func fetchedResultsController<RequestResult: NSFetchRequestResult>(_ controller: FetchedResultsController<RequestResult>, error: Error?)
}
