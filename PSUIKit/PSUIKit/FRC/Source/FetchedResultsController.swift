//
//  FetchedResultsController.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 17.11.14.
//  Copyright (c) 2014 Pavel Smelovsky. All rights reserved.
//

import UIKit
import CoreData

public enum RequireFlag {
    case reload
    case refetch
}

open class FetchedResultsController<RequestResult: NSFetchRequestResult>: UITableViewController, NSFetchedResultsControllerDelegate {

    // flag means that self.fetchedResultsController need refetch data from local storage
    private var flags: Set<RequireFlag> = []

    public var fetchedResultsController: NSFetchedResultsController<RequestResult>? {
        didSet {
            self.fetchedResultsController?.delegate = self
        }
    }

    // if controller should show some static rows
    var staticRowCount: Int = 0 {
        didSet {
            if staticRowCount > 0 && staticRowCount != oldValue {
                self.tableView.reloadData()
            }
        }
    }

    public weak var delegate: FetchedResultsControllerDelegate?

    override open func viewDidLoad() {
        super.viewDidLoad()

        // prevent showing separator under last row
        if self.tableView.tableFooterView == nil {
            self.tableView.tableFooterView = UIView()
        }

        self.initialFetch()
    }

    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Appearing

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.update(force: false)
    }

    // MARK: - Table view data source

    override open func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return self.fetchedResultsController?.sections?.count ?? 0
    }

    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = self.fetchedResultsController?.sections else {
            return 0
        }
        return sections[section].numberOfObjects + self.staticRowCount
    }

    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // will be failure if this method not overriden by subclass
        return super.tableView(tableView, cellForRowAt: indexPath)
    }

    final public func set(needs flags: RequireFlag...) {
        self.flags.removeAll()
        flags.forEach({self.flags.insert($0)})
    }

    final public func update(force: Bool = false) {
        self.refetch(force: force)
        self.reload(force: force)
    }

    private func reload(force: Bool) {
        if flags.contains(.reload) || force {
            self.delegate?.fetchedResultsController(self, loadWithCompletion: { [weak self] in
                guard let `self` = self else { return }

                // end refreshing if is in refresh mode
                if let refreshControl = self.refreshControl, refreshControl.isRefreshing {
                    refreshControl.endRefreshing()
                }
            })
        }

        flags.remove(.reload)
    }

    private func initialFetch() {
        self.fetchedResultsController = self.delegate?.fetchedResultsController(self, update: self.fetchedResultsController)

        self.performFetch()
    }

    private func refetch(force: Bool) {
        if flags.contains(.refetch) || force {
            self.initialFetch() // reconfigure fetchedResultsController and reload data

            // reload data in tableView
            self.tableView.reloadData()
        }

        flags.remove(.refetch)
    }

    @discardableResult
    private func performFetch() -> Bool {
        guard let fetchedResultsController = self.fetchedResultsController else { return false }

        do {
            try fetchedResultsController.performFetch()
            return true
        } catch let error as NSError {
            self.delegate?.fetchedResultsController(self, error: error)
            print(error.localizedDescription)
        }

        return false
    }

    public func fetchedObject(at indexPath: IndexPath) -> RequestResult? {
        let indexPath = self.indexPathForFRC(fromTableView: indexPath)
        return self.fetchedResultsController?.object(at: indexPath)
    }

    /// Translate IndexPath of UITableView to IndexPath of FetchedResultsController
    /// Difference between FRC IndexPath and UITableView is that FRC contains only dynamic part of UITableView cells
    ///
    /// - Parameter indexPath: IndexPath of UITableView
    /// - Returns: IndexPath of dynamic part of tableView cells
    private func indexPathForFRC(fromTableView indexPath: IndexPath) -> IndexPath {
        assert(indexPath.row >= self.staticRowCount, "IndexPath.row should be greater or equal to self.staticRowCount")

        if self.staticRowCount > 0 {
            return IndexPath(row: indexPath.row - self.staticRowCount, section: indexPath.section)
        }
        return indexPath
    }

    private func indexPathForTableView(fromFRC indexPath: IndexPath) -> IndexPath {
        if self.staticRowCount > 0 {
            return IndexPath(row: indexPath.row + self.staticRowCount, section: indexPath.section)
        }
        return indexPath
    }

    private func indexPathInBounds(_ indexPath: IndexPath?) -> Bool {
        guard let fetchedObjects = self.fetchedResultsController?.fetchedObjects,
            let indexPath = indexPath else {
            return false
        }

//        let res = (self.staticRowCount ..< fetchedObjects.count + self.staticRowCount).contains(indexPath.row)
        return indexPath.row >= self.staticRowCount && indexPath.row < fetchedObjects.count + self.staticRowCount
    }

    public func isEmpty() -> Bool {
        let isEmpty = fetchedResultsController?.fetchedObjects?.isEmpty ?? true

        return isEmpty && self.staticRowCount == 0
    }

    // MARK: - FRC

    // ЭТО ЖЕ ПИЗДЕЦ
    // https://openradar.appspot.com/22684413

    private var deletedSections: IndexSet!
    private var addedSections: IndexSet!

    open func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()

        self.addedSections = IndexSet()
        self.deletedSections = IndexSet()
    }

    open func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, sectionIndexTitleForSectionName sectionName: String) -> String? {
        return sectionName
    }

    open func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            self.tableView.insertSections(NSIndexSet(index: sectionIndex) as IndexSet, with: .automatic)

            self.addedSections.insert(sectionIndex)
        case .delete:
            self.tableView.deleteSections(IndexSet(integer: sectionIndex), with: UITableView.RowAnimation.automatic)

            self.deletedSections.insert(sectionIndex)
        default:
            break
        }
    }

    // swiftlint:disable cyclomatic_complexity
    open func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        if type.rawValue == 0 { return }

        let indexPath = indexPath.flatMap { (indexPath) -> IndexPath? in
            return self.indexPathForTableView(fromFRC: indexPath)
        }

        let newIndexPath = newIndexPath.flatMap { (indexPath) -> IndexPath? in
            return self.indexPathForTableView(fromFRC: indexPath)
        }

        switch type {
        case .insert:
            self.tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            self.tableView.deleteRows(at: [indexPath!], with: .automatic)
        case .update:
            if !self.deletedSections.contains(indexPath!.section) &&
                !self.addedSections.contains(indexPath!.section) {
                self.tableView.reloadRows(at: [indexPath!], with: .automatic)
            }
        case .move:
            if indexPath == newIndexPath {
                self.tableView.reloadRows(at: [indexPath!], with: .automatic)
            }
            // iOS 9.0b5 sends the same index path twice instead of delete
            else if indexPath != newIndexPath {
                self.tableView.deleteRows(at: [indexPath!], with: .automatic)
                self.tableView.insertRows(at: [newIndexPath!], with: .automatic)
            } else if self.addedSections.contains(indexPath!.section) {
                // iOS 9.0b5 bug: Moving first item from section 0 (which becomes section 1 later) to section 0
                // Really the only way is to delete and insert the same index path...
                self.tableView.deleteRows(at: [indexPath!], with: .automatic)
                self.tableView.insertRows(at: [indexPath!], with: .automatic)
            } else if self.deletedSections.contains(indexPath!.section) {
                // iOS 9.0b5 bug: same index path reported after section was removed
                // we can ignore item deletion here because the whole section was removed anyway

                self.tableView.insertRows(at: [indexPath!], with: .automatic)
            }
        @unknown default: break
        }
    }
    // swiftlint:enable cyclomatic_complexity

    open func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }

}
