//
//  RoundedImage.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 28.11.14.
//  Copyright (c) 2014 Pavel Smelovsky. All rights reserved.
//

import UIKit
import QuartzCore

public class RoundedImageView: UIImageView {
    override public func awakeFromNib() {
        super.awakeFromNib()

        self.clipsToBounds = true
        //self.layer.masksToBounds = true;
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.radius()
    }

    public func radius() -> CGFloat {
        return min(self.bounds.size.width, self.bounds.size.height) / 2.0
    }
}
