//
//  TextField.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 14.03.15.
//  Copyright (c) 2015 Pavel Smelovsky. All rights reserved.
//

import UIKit

@IBDesignable
public class TextField: UITextField {

    @IBInspectable public var placeholderColor: UIColor?
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    @IBInspectable public var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
        }
    }

    @available(iOS 8.0, *)
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()

        self.awakeFromNib()
        self.setNeedsDisplay()
    }

    public override func awakeFromNib() {
        super.awakeFromNib()

        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }

    override public func drawPlaceholder(in rect: CGRect) {
        if let color = self.placeholderColor {
            if let placeholder = self.placeholder {
                let nsstring = NSString(string: placeholder)

                var attributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: color]
                if let font = self.font {
                    attributes[NSAttributedString.Key.font] = font
                }

                let options: NSStringDrawingOptions = .usesFontLeading
                let size = nsstring.boundingRect(with: rect.size, options: options, attributes: attributes, context: nil).size

                var textRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
                textRect.origin = self.calculateTextPosition(textSize: size, inRect: rect)

                print("size: \(size), textRect: \(textRect), rect: \(rect)")

                nsstring.draw(in: textRect, withAttributes: attributes)
//                nsstring.drawWithRect(textRect, options: options, attributes: attributes, context: nil);
            }
        } else {
            super.drawPlaceholder(in: rect)
        }
    }

    private func calculateTextPosition(textSize: CGSize, inRect rect: CGRect) -> CGPoint {
        var point: CGPoint = CGPoint.zero
        point.y = rect.midY - textSize.height / 2.0

        if self.textAlignment == NSTextAlignment.center {
            point.x = rect.midX - textSize.width / 2.0
        } else if self.textAlignment == .right {
            point.x = rect.width - textSize.width
        }

        return point
    }

}
