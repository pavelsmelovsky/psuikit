//
//  Button+Colors.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 25.06.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import UIKit

extension Button: CustomBackgroundColors {
    @IBInspectable public var defaultColor: UIColor? {
        set {
            self.set(color: newValue, for: .normal)
        }
        get {
            return self.color(for: .normal)
        }
    }

    @IBInspectable public var highlightedColor: UIColor? {
        set {
            self.set(color: newValue, for: .highlighted)
        }
        get {
            return self.color(for: .highlighted)
        }
    }

    @IBInspectable public var highlightedSelectedColor: UIColor? {
        set {
            self.set(color: newValue, for: [.highlighted, .selected])
        }
        get {
            return self.color(for: [.highlighted, .selected])
        }
    }

    @IBInspectable public var selectedColor: UIColor? {
        set {
            self.set(color: newValue, for: .selected)
        }
        get {
            return self.color(for: .selected)
        }
    }

    @IBInspectable public var disabledColor: UIColor? {
        set {
            self.set(color: newValue, for: .disabled)
        }
        get {
            return self.color(for: .disabled)
        }
    }
}
