//
//  Button.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 24.12.14.
//  Copyright (c) 2014 Pavel Smelovsky. All rights reserved.
//

import UIKit

@IBDesignable
open class Button: UIButton {
    open var cornerRadius: CGFloat {
        set {
            _cornerRadius = newValue
            if self.buttonSetupComplete {
                self.didSet(cornerRadius: newValue)
            }
        }
        get { return _cornerRadius }
    }

    open var borderColor: UIColor? {
        set {
            _borderColor = newValue
            if self.buttonSetupComplete {
                self.didSet(borderColor: newValue)
            }
        }
        get { return _borderColor }
    }

    open var borderWidth: CGFloat {
        set {
            _borderWidth = newValue
            if self.buttonSetupComplete {
                self.didSet(borderWidth: newValue)
            }
        }
        get { return _borderWidth }
    }

    open var animationDuration: TimeInterval = 0.2

    override open var isHighlighted: Bool {
        didSet {
            if self.buttonSetupComplete && oldValue != isHighlighted {
                _updateUI(animated: true)
            }
        }
    }

    override open var isSelected: Bool {
        didSet {
            if self.buttonSetupComplete && oldValue != isSelected {
                _updateUI(animated: true)
            }
        }
    }

    override open var isEnabled: Bool {
        didSet {
            if self.buttonSetupComplete && oldValue != isEnabled {
                _updateUI(animated: true)
            }
        }
    }

    @IBInspectable private var _cornerRadius: CGFloat = 0
    @IBInspectable private var _borderColor: UIColor?
    @IBInspectable private var _borderWidth: CGFloat = 0

    private var strokeLayer: CALayer!

    private var backgroundView: UIView!

    private var buttonSetupComplete: Bool = false

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)

        self.commonInit()
    }

    override open func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()

        self.commonInit()
    }

    override open func awakeFromNib() {
        super.awakeFromNib()

        self.commonInit()
    }

    private func commonInit() {
        self.clipsToBounds = true

        self.layer.cornerRadius = _cornerRadius
        self.layer.masksToBounds = true
        self.layer.delegate = self

        self.backgroundView = self.createBackgroundView()
        self.insertSubview(self.backgroundView, at: 0)

        self.strokeLayer = self.createStrokeLayer()
        self.layer.insertSublayer(self.strokeLayer, at: 1)

        self.buttonSetupComplete = true
    }

    private func createBackgroundView() -> UIView {
        let view = UIView(frame: self.bounds)
        view.translatesAutoresizingMaskIntoConstraints = true
        view.isUserInteractionEnabled = false
        view.backgroundColor = self.currentColor
        view.clipsToBounds = true
        return view
    }

    private func createStrokeLayer() -> CALayer {
        let layer = CALayer()
        layer.backgroundColor = nil
        layer.masksToBounds = true

        layer.borderWidth = _borderWidth
        layer.borderColor = _borderColor?.cgColor
        layer.cornerRadius = _cornerRadius

        return layer
    }

    open override func layoutSubviews() {
        super.layoutSubviews()

        self.backgroundView.frame = self.bounds
        self.sendSubviewToBack(self.backgroundView)
    }

    override open func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)

        self.strokeLayer.frame = layer.bounds
        self.strokeLayer.cornerRadius = _cornerRadius
    }

    open func set(borderWidth: CGFloat, animated: Bool) {
        if animated {
            CATransaction.begin()
            CATransaction.setAnimationDuration(animationDuration)

            self.strokeLayer.borderWidth = borderWidth

            CATransaction.commit()
        } else {
            _invokeWithoutAnimations {
                strokeLayer.borderWidth = borderWidth
            }
        }

        _borderWidth = borderWidth
    }

    private func didSet(cornerRadius: CGFloat) {
        _invokeWithoutAnimations {
            self.layer.cornerRadius = cornerRadius
            self.strokeLayer.cornerRadius = cornerRadius
            self.backgroundView.layer.cornerRadius = cornerRadius
        }
    }

    private func didSet(borderColor: UIColor?) {
        _invokeWithoutAnimations {
            self.strokeLayer.borderColor = borderColor?.cgColor
        }
    }

    private func didSet(borderWidth: CGFloat) {
        _invokeWithoutAnimations {
            self.strokeLayer.borderWidth = borderWidth
        }
    }

    private func _invokeWithoutAnimations(_ block: () -> Void) {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        block()
        CATransaction.setDisableActions(false)
        CATransaction.commit()
    }

    private func _updateUI(animated: Bool) {
        let color = self.currentColor

        debugPrint("Button state: \(self.state.debugDescription)")
        debugPrint("Button color: \(color.debugDescription)")

        // FIXME: if backgroundColor is nil, then animation does not work
        if self.backgroundView.backgroundColor == nil {
            self.backgroundView.backgroundColor = .clear
        }

        let animations: () -> Void = {
            self.backgroundView.backgroundColor = color
        }

        if animated {
            let options: UIView.AnimationOptions = [.beginFromCurrentState, .curveEaseOut]
            UIView.animate(withDuration: animationDuration, delay: 0, options: options, animations: animations, completion: nil)
        } else {
            animations()
        }
    }

    // MARK: - Background colors

    open func color(didChange color: UIColor?, for state: UIControl.State) {
        if state == self.state, let view = backgroundView {
            view.backgroundColor = color
        }
    }

    // MARK: Show activity indicator

    private var activityIndicator: UIActivityIndicatorView? {
        didSet {
            if let oldValue = oldValue {
                oldValue.removeFromSuperview()
            }

            if let activityIndicator = activityIndicator {
                self.addSubview(activityIndicator)
            }
        }
    }

    open func showActivityIndicator() {
        //self.setImage(, forState: UIControlState.Disabled);
        self.isEnabled = false

        if let indicator = activityIndicator {
            indicator.removeFromSuperview()
        }

        let indicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.white)
        indicator.color = self.tintColor
        indicator.sizeToFit()

        if let imageView = self.imageView, imageView.image != nil, imageView.superview != nil {
            indicator.center = imageView.center

            // remove imageView from view's hierarhy to hide image from button
            imageView.removeFromSuperview()
        } else {
            let center = CGPoint(x: bounds.width / 2.0,
                                 y: bounds.height / 2.0)
            indicator.center = center
        }

        activityIndicator = indicator
        indicator.startAnimating()
    }

    open func hideActivityIndicator() {
        activityIndicator = nil

        if let imageView = self.imageView {
            self.addSubview(imageView)
        }

        self.isEnabled = true
    }

    // MARK: - First responder

    @IBInspectable open var firstResponder: Bool = false

    private var _inputView: UIView?
    open override var inputView: UIView? {
        get { return _inputView }
        set { _inputView = newValue }
    }

    private var _inputAccessoryView: UIView?
    open override var inputAccessoryView: UIView? {
        get { return _inputAccessoryView }
        set { _inputAccessoryView = newValue }
    }

    override open var canBecomeFirstResponder: Bool {
        return self.firstResponder
    }

    override open var canResignFirstResponder: Bool {
        return self.firstResponder
    }
}
