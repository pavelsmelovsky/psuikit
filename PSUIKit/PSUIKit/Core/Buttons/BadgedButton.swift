//
//  SPBadgedButton.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 30.04.16.
//  Copyright © 2016 Pavel Smelovsky. All rights reserved.
//

import UIKit

@IBDesignable
public class BadgedButton: UIButton {
    @IBInspectable public var badgeValue: String? {
        didSet {
            if let badgeValue = self.badgeValue, !badgeValue.isEmpty {
                self._initializeBadgeLabel()
                self.badgeLabel?.text = self.badgeValue
            } else {
                self._clearBadgeLabel()
            }

            self.setNeedsLayout()
        }
    }

    @IBInspectable public var badgeBackgroundColor: UIColor = UIColor(red: 0xff / 255.0, green: 0x3b / 255.0, blue: 0x30 / 255.0, alpha: 1.0) {
        didSet {
            self.badgeLabel?.backgroundColor = self.badgeBackgroundColor
        }
    }

    @IBInspectable public var badgeTextColor: UIColor = UIColor.white {
        didSet {
            self.badgeLabel?.textColor = self.badgeTextColor
        }
    }

    public var badgeFont: UIFont = UIFont.systemFont(ofSize: 11) {
        didSet {
            if let badgeLabel = self.badgeLabel {
                badgeLabel.font = self.badgeFont
            }
        }
    }

    override open var isHighlighted: Bool {
        didSet {
            self.badgeLabel?.isHighlighted = self.isHighlighted
        }
    }

    /**
     Minimum height of badge label.
     When badge label contains only one short symbol, height == diameter
     */
    @IBInspectable public var minimumHeight: CGFloat = 16.0

    private weak var badgeLabel: UILabel?

    private func initializeButtonWithTitle(title: String?, image: UIImage?, landscapeImagePhone: UIImage?, target: Any?, action: Selector) -> UIButton {
        let button = UIButton()

        button.setTitle(title, for: .normal)
        button.setImage(image, for: .normal)

        if let target = target {
            button.addTarget(target, action: action, for: .touchUpInside)
        }

        button.sizeToFit()

        return button
    }

    @available(iOS 8.0, *)
    override open func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()

        self.setNeedsLayout()
        self.layoutIfNeeded()
    }

    override open func layoutSubviews() {
        super.layoutSubviews()

        if let badgeLabel = self.badgeLabel, !badgeLabel.isHidden {
            let rect = self.frame

            badgeLabel.sizeToFit()

            var labelRect = badgeLabel.bounds

            let labelHeight = labelRect.height

            // calculate diameter of rounded UILabel
            let oneCharLabelDiameter = sqrt(pow(labelHeight, 2) + pow(labelHeight / 2.0, 2))

            // calculate diameter of badge
            let diameter = self.minimumHeight < oneCharLabelDiameter ? oneCharLabelDiameter : self.minimumHeight

            // padding from border of UILabel to text
            let padding = diameter / 4.0

            labelRect.size.height = diameter
            labelRect.size.width = (labelRect.width + padding * 2) < diameter ? diameter : labelRect.width + padding * 2

            labelRect.origin.x = rect.width - labelRect.width + diameter / 2.0
            labelRect.origin.y = -diameter / 2.0

            badgeLabel.frame = labelRect

            badgeLabel.layer.cornerRadius = diameter / 2.0

            badgeLabel.setNeedsDisplay()
        }
    }

    private func _initializeBadgeLabel() {
        if self.badgeLabel == nil {
            let badgeLabel = UILabel()

            badgeLabel.font = self.badgeFont
            badgeLabel.backgroundColor = self.badgeBackgroundColor
            badgeLabel.textColor = self.badgeTextColor
            badgeLabel.textAlignment = .center

            badgeLabel.layer.cornerRadius = 8
            badgeLabel.layer.masksToBounds = true

            badgeLabel.clipsToBounds = true

            self.addSubview(badgeLabel)

            self.badgeLabel = badgeLabel
        }
    }

    private func _clearBadgeLabel() {
        self.badgeLabel?.removeFromSuperview()
    }
}
