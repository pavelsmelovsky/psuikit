//
//  SPTopImageButton.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 30.01.15.
//  Copyright (c) 2015 Pavel Smelovsky. All rights reserved.
//

import UIKit

//@IBDesignable
public class TopImageButton: Button {
    //@IBInspectable var space:

    private weak var _imageView: UIImageView?
    private weak var _titleLabel: UILabel?

    @IBInspectable public var numberOfLines: Int = 1 {
        didSet {
            _titleLabel?.numberOfLines = self.numberOfLines
        }
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        _titleLabel = self.titleLabel
        _imageView = self.imageView
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)

        _titleLabel = self.titleLabel
        _imageView = self.imageView
    }

    override public func awakeFromNib() {
        super.awakeFromNib()
    }

    override public func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.awakeFromNib()
        self.setNeedsLayout()
    }

    override public func backgroundRect(forBounds bounds: CGRect) -> CGRect {
        return super.backgroundRect(forBounds: bounds)
    }

    override public func contentRect(forBounds bounds: CGRect) -> CGRect {
        return super.contentRect(forBounds: bounds)
    }

    override public func layoutSubviews() {
        super.layoutSubviews()

        guard let imageView = _imageView,
                let titleLabel = _titleLabel else {
            return
        }

        let contentRect = self.contentRect(forBounds: self.bounds)

        var imageFrame = CGRect.zero
        imageFrame.size = imageView.intrinsicContentSize

        var titleFrame = CGRect.zero
        let maxWidth = contentRect.width - (self.titleEdgeInsets.left + self.titleEdgeInsets.right)
        titleFrame.size = titleLabel.sizeThatFits(CGSize(width: maxWidth, height: -1))

        self.layout(imageFrame: &imageFrame, titleFrame: &titleFrame, forVerticalAlignment: self.contentVerticalAlignment, inContentRect: contentRect)

        var textAlignment: NSTextAlignment = .justified
        self.layout(imageFrame: &imageFrame, titleFrame: &titleFrame, textAlignment: &textAlignment, forHorizontalAlignment: self.contentHorizontalAlignment, inContentRect: contentRect)

        imageView.frame = imageFrame

        titleLabel.frame = titleFrame
        titleLabel.textAlignment = textAlignment
    }

    private func layout(imageFrame: inout CGRect, titleFrame: inout CGRect, forVerticalAlignment alignment: UIControl.ContentVerticalAlignment, inContentRect contentRect: CGRect) {
        let imageVerticalInsets = self.imageEdgeInsets.top + self.imageEdgeInsets.bottom
        let titleVerticalInsets = self.titleEdgeInsets.top + self.titleEdgeInsets.bottom

        let contentHeight = imageFrame.height + titleFrame.height + imageVerticalInsets + titleVerticalInsets

        switch alignment {
        case .top:
            imageFrame.origin.y = contentRect.minY + self.imageEdgeInsets.top
            titleFrame.origin.y = imageFrame.maxY + self.imageEdgeInsets.bottom + self.titleEdgeInsets.top
        case .center:
            let yTop = (contentRect.height - contentHeight) / 2.0
            imageFrame.origin.y = contentRect.minY + self.imageEdgeInsets.top + yTop

            titleFrame.origin.y = imageFrame.maxY + self.imageEdgeInsets.bottom + self.titleEdgeInsets.top
        case .bottom:
            let yBottom = contentRect.maxY - self.titleEdgeInsets.bottom
            titleFrame.origin.y = yBottom - titleFrame.height

            imageFrame.origin.y = titleFrame.minY - imageFrame.height - (self.imageEdgeInsets.bottom + self.titleEdgeInsets.top)
        case .fill:
            // at this alignment mode - align text on the bottom of view and image between top edge and text
            let yBottom = contentRect.maxY - self.titleEdgeInsets.bottom
            titleFrame.origin.y = yBottom - titleFrame.height

            let availableSpace = contentRect.height - titleFrame.height - titleVerticalInsets

            let middlePoint = (availableSpace - imageFrame.height) / 2.0
            imageFrame.origin.y = contentRect.minY + self.imageEdgeInsets.top + middlePoint
        @unknown default: break
        }
    }

    private func layout(imageFrame: inout CGRect, titleFrame: inout CGRect, textAlignment: inout NSTextAlignment, forHorizontalAlignment alignment: UIControl.ContentHorizontalAlignment, inContentRect contentRect: CGRect) {
        switch alignment {
        case .left:
            imageFrame.origin.x = self.contentEdgeInsets.left + self.imageEdgeInsets.left
            titleFrame.origin.x = self.contentEdgeInsets.left + self.titleEdgeInsets.left

            textAlignment = .left
        case .center:
            imageFrame.origin.x = (self.frame.width - imageFrame.width) / 2.0
            titleFrame.origin.x = (self.frame.width - titleFrame.width) / 2.0

            textAlignment = .center
        case .right:
            imageFrame.origin.x = self.frame.width - imageFrame.width - (self.contentEdgeInsets.right + self.imageEdgeInsets.right)

            titleFrame.origin.x = self.frame.width - titleFrame.width - (self.contentEdgeInsets.right + self.titleEdgeInsets.right)

            textAlignment = .right
        case .fill:
            imageFrame.origin.x = (self.frame.width - imageFrame.width) / 2.0

            titleFrame.origin.x = self.contentEdgeInsets.left + self.titleEdgeInsets.left
            titleFrame.size.width = contentRect.width - (self.titleEdgeInsets.left + self.titleEdgeInsets.right)
            textAlignment = .justified
        case .leading, .trailing:
            if #available(iOS 11, *) {
                let direction = UIView.userInterfaceLayoutDirection(for: self.semanticContentAttribute)

                let newAlignment: UIControl.ContentHorizontalAlignment
                if (direction == .leftToRight && alignment == .leading) || (direction == .rightToLeft && alignment == .trailing) {
                    newAlignment = .left
                } else if (direction == .rightToLeft && alignment == .leading) || (direction == .leftToRight && alignment == .trailing) {
                    newAlignment = .right
                } else {
                    newAlignment = .center
                }

                self.layout(imageFrame: &imageFrame, titleFrame: &titleFrame, textAlignment: &textAlignment, forHorizontalAlignment: newAlignment, inContentRect: contentRect)
            }
        @unknown default: break
        }
    }

    public override var intrinsicContentSize: CGSize {
        let originalSize = super.intrinsicContentSize

        guard let imageView = _imageView,
            let titleLabel = _titleLabel else {
            return originalSize
        }

        var newSize: CGSize = originalSize

        let imageSize = imageView.intrinsicContentSize //frame.size
        let labelSize = titleLabel.intrinsicContentSize //frame.size

        let imageHorizontalInsets = self.imageEdgeInsets.left + self.imageEdgeInsets.right
        let imageVerticalInsets = self.imageEdgeInsets.top + self.imageEdgeInsets.bottom

        let titleHorizontalInsets = self.titleEdgeInsets.left + self.titleEdgeInsets.right
        let titleVerticalInsets = self.titleEdgeInsets.top + self.titleEdgeInsets.bottom

        let contentHorizontalInsets = self.contentEdgeInsets.left + self.contentEdgeInsets.right
        let contentVerticalInsets = self.contentEdgeInsets.top + self.contentEdgeInsets.bottom

        //if originalSize.width == -1
        //{
        newSize.width = fmax(imageSize.width + imageHorizontalInsets, labelSize.width + titleHorizontalInsets) + contentHorizontalInsets
        //}

        //if originalSize.height == -1
        //{
        titleLabel.sizeToFit()
        let bestLabelSize = titleLabel.sizeThatFits(CGSize(width: newSize.width - titleHorizontalInsets, height: -1))
        newSize.height = imageSize.height + imageVerticalInsets + bestLabelSize.height + titleVerticalInsets + contentVerticalInsets
        //}

        return newSize
    }
}
