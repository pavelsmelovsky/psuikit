//
//  Label.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 02.03.15.
//  Copyright (c) 2015 Pavel Smelovsky. All rights reserved.
//

import UIKit

@IBDesignable
public class Label: UILabel {
    public var contentInsets: UIEdgeInsets = UIEdgeInsets(top: 1, left: 8, bottom: 1, right: 8)

    @IBInspectable public var cornerRadius: CGFloat = 0 {
        didSet {
            self.clipsToBounds = true
            self.layer.masksToBounds = true

            self.layer.cornerRadius = cornerRadius
        }
    }

    @IBInspectable public var calculateCorner: Bool = false {
        didSet {
            self._calculateCornerRadius()
        }
    }

    @IBInspectable public var showBorder: Bool = false {
        didSet {
            if self.showBorder {
                self.layer.borderWidth = self.borderWidth
                self._updateBorderColor()
            } else {
                self.layer.borderWidth = 0.0
            }
        }
    }

    @IBInspectable public var borderColor: UIColor? {
        didSet {
            if self.showBorder {
                self._updateBorderColor()
            }
        }
    }

    @IBInspectable public var highlightedBorderColor: UIColor? {
        didSet {
            if self.showBorder {
                self._updateBorderColor()
            }
        }
    }

    @IBInspectable public var borderWidth: CGFloat = 1.0 {
        didSet {
            if self.showBorder {
                self.layer.borderWidth = borderWidth
                self._updateBorderColor()
            }
        }
    }

    override public var bounds: CGRect {
        didSet {
            self.preferredMaxLayoutWidth = bounds.width

            if self.calculateCorner {
                self._calculateCornerRadius()
            }
        }
    }

    override public var isHighlighted: Bool {
        didSet {
            if self.showBorder {
                self._updateBorderColor()
            }
        }
    }

    @available(iOS 8.0, *)
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()

        self.awakeFromNib()
        self.setNeedsLayout()
    }

    public override func awakeFromNib() {
        super.awakeFromNib()
    }

    private func _updateBorderColor() {
        var color: UIColor?

        if self.isHighlighted {
            color = self.highlightedBorderColor ?? self.highlightedTextColor
        } else {
            color = self.borderColor ?? self.textColor
        }

        self.layer.borderColor = color?.cgColor
    }

    private func _calculateCornerRadius() {
        self.cornerRadius = fmin(self.bounds.width, self.bounds.height) / 2.0
    }

    private func textContentRect(bounds: CGRect) -> CGRect {
        var rect = CGRect.zero
        rect.origin.x = self.contentInsets.left
        rect.origin.y = self.contentInsets.top
        rect.size.width = bounds.width - (self.contentInsets.left + self.contentInsets.right)
        rect.size.height = bounds.height - (self.contentInsets.top + self.contentInsets.bottom)
        return rect
    }

    public override func drawText(in rect: CGRect) {
        let newRect = self.textContentRect(bounds: rect)
        print("draw text in rect \(newRect)")
        super.drawText(in: newRect)
    }

    public override var intrinsicContentSize: CGSize {
        var size = super.intrinsicContentSize
        size.width += self.contentInsets.left + self.contentInsets.right
        size.height += self.contentInsets.top + self.contentInsets.bottom
        print("Intrinsic size: \(size)")
        return size
    }

    public override func sizeToFit() {
        super.sizeToFit()
        print("sizeToFit: \(self.bounds)")
    }
}
