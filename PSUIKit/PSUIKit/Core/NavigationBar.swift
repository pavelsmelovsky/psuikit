//
//  NavigationBar.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 30.06.16.
//  Copyright © 2016 Smelovsky.Me. All rights reserved.
//

import UIKit

open class NavigationBar: UINavigationBar {
    // from here: http://stackoverflow.com/questions/17683970/why-is-the-top-portion-of-my-uisegmentedcontrol-not-tappable

    private var lastOutOfBoundsEventTimestamp: TimeInterval = 0

    override open func awakeFromNib() {
        super.awakeFromNib()
    }

    override open func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return super.point(inside: point, with: event)
    }

    override open func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        guard let event = event else {
            return super.hitTest(point, with: nil)
        }

        // [rfillion 2014-03-28]
        // UIApplication/UIWindow/UINavigationBar conspire against us. There's a band under the UINavigationBar for which the bar will return
        // subviews instead of nil (to make those tap targets larger, one would assume). We don't want that. To do this, it seems to end up
        // calling -hitTest twice. Once with a value out of bounds which is easy to check for. But then it calls it again with an altered point
        // value that is actually within bounds. The UIEvent it passes to both seem to be the same. However, we can't just compare UIEvent pointers
        // because it looks like these get reused and you end up rejecting valid touches if you just keep around the last bad touch UIEvent. So
        // instead we keep around the timestamp of the last bad event, and try to avoid processing any events whose timestamp isn't larger.
        if point.y > self.bounds.size.height {
            self.lastOutOfBoundsEventTimestamp = event.timestamp
            return nil
        }

        if event.timestamp <= self.lastOutOfBoundsEventTimestamp + 0.001 {
            return nil
        }

        return super.hitTest(point, with: event)
    }
}
