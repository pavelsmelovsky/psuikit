//
//  CGImageFunctions.c
//  PSUIKit
//
//  Created by Pavel Smelovsky on 09.05.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

#import "CGImageFunctions.h"
#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

CGRect CGRectForImage(CGImageRef image, ImageMode imageMode, CGRect bounds) {
    switch (imageMode) {
        case ImageModeFill:
            return bounds;
        case ImageModeAspectFit:
            return CGAspectFitImageSize(image, bounds);
        case ImageModeAspectFill:
            return CGAspectFillImageSize(image, bounds);
    }
}

CGSize CGSizeForImage(CGImageRef image) {
    return CGSizeMake(CGImageGetWidth(image),
                      CGImageGetHeight(image));
}

CGRect CGAspectFitImageSize(CGImageRef image, CGRect bounds) {
    CGSize imageSize = CGSizeForImage(image);

    CGFloat ratio = MIN(CGRectGetWidth(bounds) / imageSize.width,
                        CGRectGetHeight(bounds) / imageSize.height);
    CGAffineTransform transform = CGAffineTransformMakeScale(ratio, ratio);
    imageSize = CGSizeApplyAffineTransform(imageSize, transform);
    return CGRectInscribeSize(bounds, imageSize);
}

CGRect CGAspectFillImageSize(CGImageRef image, CGRect bounds) {
    CGSize imageSize = CGSizeForImage(image);

    CGFloat ratio = MAX(CGRectGetWidth(bounds) / imageSize.width,
                        CGRectGetHeight(bounds) / imageSize.height);
    CGAffineTransform transform = CGAffineTransformMakeScale(ratio, ratio);
    imageSize = CGSizeApplyAffineTransform(imageSize, transform);
    return CGRectInscribeSize(bounds, imageSize);
}

CGRect CGRectInscribeSize(CGRect rect, CGSize size) {
    CGRect resultRect = rect;
    resultRect.size = size;

    // centrify image
    resultRect.origin.x = (CGRectGetWidth(rect) - CGRectGetWidth(resultRect)) / 2.0;
    resultRect.origin.y = (CGRectGetHeight(rect) - CGRectGetHeight(resultRect)) / 2.0;
    return resultRect;
}
