//
//  CGImageFunctions.h
//  PSUIKit
//
//  Created by Pavel Smelovsky on 09.05.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

#ifndef CGImageFunctions_h
#define CGImageFunctions_h

@import Foundation;
@import CoreGraphics;

typedef NS_ENUM(NSUInteger, ImageMode) {
    ImageModeFill,
    ImageModeAspectFit,
    ImageModeAspectFill,
};

CGRect CGRectForImage(CGImageRef image, ImageMode imageMode, CGRect bounds);
CGSize CGSizeForImage(CGImageRef image);
CGRect CGAspectFitImageSize(CGImageRef image, CGRect bounds);
CGRect CGAspectFillImageSize(CGImageRef image, CGRect bounds);
CGRect CGRectInscribeSize(CGRect rect, CGSize size);

#endif /* CGImageFunctions_h */
