//
//  SegmentedToolbar.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 02.07.16.
//  Copyright © 2016 Smelovsky.Me. All rights reserved.
//

import UIKit

public enum SegmentAlignment: Int {
    case left
    case right
    case center
    case justify
}

open class SegmentedToolbar: UIToolbar {
    @IBOutlet public private(set) var segmentedItem: UIBarButtonItem! {
        didSet {
            self.alignItems()
        }
    }

    public var segmentAlignment: SegmentAlignment = .justify {
        didSet {
            self.alignItems()
        }
    }

    public var insets: UIEdgeInsets = UIEdgeInsets(top: 1, left: 16, bottom: 1, right: 16)

    public init(items: [String]) {
        super.init(frame: CGRect.zero)
        self.addSegmentedControl(items: items)
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func addSegmentedControl(items: [String]) {
        let control = UISegmentedControl(items: items)
        control.tintColor = UIColor.white
        control.selectedSegmentIndex = 0

        self.segmentedItem = UIBarButtonItem(customView: control)
    }

    open override func awakeFromNib() {
        super.awakeFromNib()

        precondition(self.segmentedItem != nil, "Make outlet for segmentedItem from your interface builder")

        guard self.segmentedItem.customView is UISegmentedControl else {
            preconditionFailure("Custom view of segmentedItem must be a valid UISegmentedControl object")
        }
    }

    private func alignItems() {
        switch self.segmentAlignment {
        case .justify, .center:
            self.alignJustified()
        case .left:
            self.alignLeft()
        case .right:
            self.alignRight()
        }
    }

    private func alignJustified() {
        let leftSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let rightSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)

        self.items = [leftSpace, self.segmentedItem, rightSpace]
    }

    private func alignLeft() {
        let leftSpace = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        leftSpace.width = self.insets.left

        self.items = [leftSpace, self.segmentedItem]
    }

    private func alignRight() {
        let rightSpace = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        rightSpace.width = self.insets.right

        self.items = [self.segmentedItem, rightSpace]
    }

    // MARK: - layout

    override open func layoutSubviews() {
        super.layoutSubviews()

        if let segmentedControl = segmentedItem.customView as? UISegmentedControl {
            if self.segmentAlignment == .justify {
                let segmentFrame = self.bounds.inset(by: self.insets)
                segmentedControl.bounds.size.width = segmentFrame.width
            } else {
                segmentedControl.sizeToFit()
            }
        }
    }

    // MARK: - Under NavBar fix

    private var touchInterval: TimeInterval = 0

    override open func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        guard let event = event else {
            return super.hitTest(point, with: nil)
        }

        guard let segmentedControl = segmentedItem.customView as? UISegmentedControl else {
            return nil
        }

        if self.isHidden
            || self.superview == nil
            || !self.isUserInteractionEnabled
            || !segmentedControl.isUserInteractionEnabled {
            return nil
        }

        // Если контрол находится под навбаром, то система дважды проверяет точку, где было нажатие
        // НавБар перехватывает примерно 16 пикселей под ним для своих нужд
        // Проверяем, если нажатие произошло в области segmentedControl'а
        // запоминаем штамп времени
        if segmentedControl.frame.contains(point) {
            self.touchInterval = event.timestamp
            return segmentedControl
        }

        // если повторная проверка происходит с минимальной разницей
        // тогда вновь возвращаем segmentedControl
        if event.timestamp <= self.touchInterval + 0.001 {
            return segmentedControl
        }

        return super.hitTest(point, with: event)
    }
}
