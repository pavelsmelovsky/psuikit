//
//  NSWeakReference.m
//  PSUIKit
//
//  Created by Pavel Smelovsky on 09.05.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

#import "NSWeakReference.h"

@interface NSWeakReference()

@property (nonatomic, weak) id object;

@end

#pragma mark - Implementation

@implementation NSWeakReference

+ (instancetype)weakReference:(id)object {
    return [[self alloc] initWithObject:object];
}

- (instancetype)initWithObject:(id)object {
    if (self = [super init]) {
        self.object = object;
    }
    return self;
}

@end
