//
//  NSWeakReference.h
//  PSUIKit
//
//  Created by Pavel Smelovsky on 09.05.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSWeakReference : NSObject

+ (instancetype)weakReference:(id)object;

@property (nonatomic, weak, readonly) id object;

@end
