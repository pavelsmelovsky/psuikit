//
//  UIControlState+Description.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 27.06.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import UIKit

extension UIControl.State: CustomDebugStringConvertible {
    public var debugDescription: String {
        var resultArray: [String] = []

        let states: [UIControl.State] = [.normal, .highlighted, .selected, .disabled]
        for state in states {
            if self.contains(state) {
                switch state {
                case .normal: resultArray.append(".normal")
                case .highlighted: resultArray.append(".highlighted")
                case .selected: resultArray.append(".selected")
                case .disabled: resultArray.append(".disabled")
                default: continue
                }
            }
        }

        if #available(iOS 9, *) {
            if self.contains(.focused) {
                resultArray.append(".focused")
            }
        }

        return resultArray.joined(separator: ", ")
    }
}
