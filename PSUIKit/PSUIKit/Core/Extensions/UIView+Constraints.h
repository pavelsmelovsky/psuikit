//
//  UIView+Constraints.h
//  PSUIKit
//
//  Created by Pavel Smelovsky on 02.04.15.
//  Copyright (c) 2015 Pavel Smelovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Constraints)

- (NSLayoutConstraint *)widthConstraint;
- (NSLayoutConstraint *)heightConstraint;

- (void)setWidthConstraint:(CGFloat)constant;
- (void)setHeightConstraint:(CGFloat)constant;

- (void)updateWidthConstraint:(CGFloat)constant;
- (void)updateHeightConstraint:(CGFloat)constant;



- (NSLayoutConstraint *)centerXConstraint;

- (NSLayoutConstraint *)centerXConstraintForView:(UIView *)view;

- (NSLayoutConstraint *)centerYConstraint;

- (NSLayoutConstraint *)centerYConstraintForView:(UIView *)view;

- (NSLayoutConstraint *)constraintWithFirstAttribute:(NSLayoutAttribute)firstAttribute secondAttribute:(NSLayoutAttribute)secondAttribute secondItem:(UIView *)secondItem;



- (NSArray *)setCenterConstraintsToView:(UIView *)view;

- (NSLayoutConstraint *)setCenterXConstraintToView:(UIView *)view constant:(CGFloat)constant;

- (NSLayoutConstraint *)setCenterYConstraintToView:(UIView *)view constant:(CGFloat)constant;

@end
