//
//  UIColor+Hex.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 24.02.16.
//  Copyright © 2016 Pavel Smelovsky. All rights reserved.
//

import UIKit

extension UIColor {
    convenience public init(color: Int) {
        self.init(color: color, alpha: 1.0)
    }

    convenience public init(color: Int, useAlpha: Bool) {
        let alpha = useAlpha ? (color >> 24) & 0x000000FF : 0xFF

        self.init(color: color, alpha: CGFloat(alpha) / 255.0)
    }

    /**
     Create UIColor object with specified color (as Int object) and alpha channel
     
     - parameter color: Color as Int, maybe hex number like 0xFF00FF
     - parameter alpha: The opacity value of the color object, specified as a value from 0.0 to 1.0.
     
     - returns: An initialized color object
     */
    convenience public init(color: Int, alpha: CGFloat) {
        let red = (color >> 16) & 0x000000FF
        let green = (color >> 8) & 0x000000FF
        let blue = color & 0x000000FF

        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alpha)
    }
}
