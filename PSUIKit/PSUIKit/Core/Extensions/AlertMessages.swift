//
//  AlertMessages.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 23.10.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import UIKit

public typealias AlertDismissed = () -> Swift.Void

public protocol AlertMessages {
    func alert(title: String?, message: String?)

    func alert(title: String?, message: String?, callback: AlertDismissed?)

    func alert(error: Error)
}

public extension AlertMessages where Self: UIViewController {
    func alert(title: String?, message: String?) {
        self.alert(title: title, message: message, callback: nil)
    }

    func alert(title: String?, message: String?, callback: AlertDismissed?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let okAction = UIAlertAction(title: "Ok", style: .default, handler: { _ in
            callback?()
        })
        alert.addAction(okAction)

        self.present(alert, animated: true, completion: nil)
    }

    func alert(error: Error) {
        if let error = error as NSError? {
            self.alert(nsError: error)
        } else {
            self.alert(title: error.localizedDescription, message: nil)
        }
    }

    private func alert(nsError error: NSError) {
        self.alert(title: error.localizedDescription, message: error.localizedFailureReason)
    }
}
