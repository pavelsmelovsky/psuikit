//
//  UIImage+Color.h
//  PSUIKit
//
//  Created by Pavel Smelovsky on 02.04.15.
//  Copyright (c) 2015 Pavel Smelovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
