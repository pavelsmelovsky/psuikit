//
//  UITableViewCell_DynamicHeightExt.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 30.03.16.
//  Copyright © 2016 Pavel Smelovsky. All rights reserved.
//

import UIKit

extension UITableViewCell {
    @available(iOS 8, *)
    public func calculateSize() -> CGSize {
        guard let tableView = self.tableView else {
            preconditionFailure("Cannot find UITableView in Hierarchy")
        }

        let width = tableView.bounds.width - (tableView.contentInset.left + tableView.contentInset.right)

        let targetSize = CGSize(width: width, height: 0)
        let horizontalPriority = UILayoutPriority.required
        let verticalPriority = Int(UILayoutPriority.defaultHigh.rawValue) + 1

        let size = self.contentView.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalPriority, verticalFittingPriority: UILayoutPriority(rawValue: UILayoutPriority.RawValue(verticalPriority)))

        return size
    }
}
