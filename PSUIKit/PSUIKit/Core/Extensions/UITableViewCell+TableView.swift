//
//  UITableViewCell_TableView.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 30.03.16.
//  Copyright © 2016 Pavel Smelovsky. All rights reserved.
//

import UIKit
import ObjectiveC

private let kCellTableViewKey = malloc(4)!

public extension UITableViewCell {
    private var _tableView: UITableView? {
        get {
            return objc_getAssociatedObject(self, kCellTableViewKey) as? UITableView
        }
        set {
            objc_setAssociatedObject(self, kCellTableViewKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    var tableView: UITableView? {
        if _tableView == nil {
            _tableView = _findTableView(fromView: self.superview)
        }

        return _tableView
    }

    private func _findTableView(fromView view: UIView?) -> UITableView? {
        var view = view
        while view != nil && !(view is UITableView) {
            view = view?.superview
        }
        return view as? UITableView
    }
}
