//
//  UIImage+Scaling.m
//  PSUIKit
//
//  Created by Pavel Smelovsky on 30.09.15.
//  Copyright © 2015 Pavel Smelovsky. All rights reserved.
//

#import "UIImage+Scaling.h"

@implementation UIImage (Scaling)

- (CGSize)convertSizeToMatchImageAspectRatio:(CGSize)size
{
    CGFloat maxSize = MAX(size.width, size.height);
    CGFloat minSize = MIN(size.width, size.height);
    
    CGFloat originalWidth = self.size.width;
    CGFloat originalHeight = self.size.height;
    
    CGFloat maxImageSize = MAX(originalWidth, originalHeight);
    CGFloat minImageSize = MIN(originalWidth, originalHeight);
    
    CGFloat scaleFactor = MIN(maxSize / maxImageSize, minSize / minImageSize);
    
    return CGSizeMake(size.width * scaleFactor, size.height * scaleFactor);
}

- (UIImage *)scaleToSize:(CGSize)size
{
    CGImageRef image = self.CGImage;
    
    size = [self convertSizeToMatchImageAspectRatio:size];

    size_t bitsPerComponent = CGImageGetBitsPerComponent(image);
    size_t bytesPerRow = CGImageGetBytesPerRow(image);
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image);
    CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(image);
    
    CGContextRef context = CGBitmapContextCreate(nil, size.width, size.height, bitsPerComponent, bytesPerRow, colorSpace, bitmapInfo);
    
    CGContextSetInterpolationQuality(context, kCGInterpolationMedium);
    
    CGContextDrawImage(context, CGRectMake(0, 0, size.width, size.height), image);
    
    return [UIImage imageWithCGImage:CGBitmapContextCreateImage(context)];
}

@end
