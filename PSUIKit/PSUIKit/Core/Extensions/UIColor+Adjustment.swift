//
//  UIColor+Adjustment.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 27.06.2018.
//  Copyright © 2018 Onza.Me. All rights reserved.
//

import UIKit

enum ColorAdjustment {
    case lighter
    case darker
}

extension UIColor {
    func adjust(_ adjustment: ColorAdjustment) -> UIColor {
        let constant: CGFloat = 1.4

        var hue: CGFloat = 0
        var saturation: CGFloat = 0
        var brightness: CGFloat = 0
        var alpha: CGFloat = 0
        self.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)

        switch adjustment {
        case .lighter:
            brightness = min(1, brightness * constant)
        case .darker:
            brightness /= constant
        }

        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: alpha)
    }
}
