//
//  UIViewController+Rotation.h
//  SPFoundation
//
//  Created by Pavel Smelovsky on 18.02.16.
//  Copyright © 2016 Pavel Smelovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Rotation)

@property (nonatomic, assign) BOOL allowRotation;

@end
