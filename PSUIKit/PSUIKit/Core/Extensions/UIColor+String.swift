//
//  UIColor+Hex.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 24.02.16.
//  Copyright © 2016 Pavel Smelovsky. All rights reserved.
//

import UIKit

extension UIColor {
    convenience public init(color: String) {
        self.init(color: color, alpha: 1.0)
    }

//    convenience public init(color: String, useAlpha: Bool) {
//        let alpha = useAlpha ? (color >> 24) & 0x000000FF : 0xFF
//
//        self.init(color: color, alpha: CGFloat(alpha) / 255.0)
//    }

    /**
     Create UIColor object with specified color (as Int object) and alpha channel
     
     - parameter color: Color as Int, maybe hex number like 0xFF00FF
     - parameter alpha: The opacity value of the color object, specified as a value from 0.0 to 1.0.
     
     - returns: An initialized color object
     */
    convenience public init(color: String, alpha: CGFloat) {
        var value: UInt32 = 0
        withUnsafeMutablePointer(to: &value) { (pointer) -> Swift.Void in
            let scanner = Scanner(string: color)
            scanner.scanHexInt32(pointer)
        }

        self.init(color: Int(value), alpha: alpha)
    }
}
