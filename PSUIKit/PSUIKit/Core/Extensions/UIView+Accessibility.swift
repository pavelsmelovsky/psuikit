//
//  UIView+Accessibility.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 18.09.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import UIKit

public extension UIView {
    @IBInspectable var ignoresInvertColors: Bool {
        set {
            if #available(iOS 11.0, *) {
                self.accessibilityIgnoresInvertColors = newValue
            }
        }
        get {
            if #available(iOS 11.0, *) {
                return self.accessibilityIgnoresInvertColors
            } else {
                return false
            }
        }
    }
}
