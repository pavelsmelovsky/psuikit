//
//  UIViewController+Rotation.m
//  SPFoundation
//
//  Created by Pavel Smelovsky on 18.02.16.
//  Copyright © 2016 Pavel Smelovsky. All rights reserved.
//

#import "UIViewController+Rotation.h"

#import <objc/runtime.h>

static void *allowRotationKey = &allowRotationKey;

@implementation UIViewController (Rotation)

- (BOOL)allowRotation
{
    id res = objc_getAssociatedObject(self, allowRotationKey);
    return res != nil ? [res boolValue] : NO;
}

- (void)setAllowRotation:(BOOL)allowRotation
{
    objc_setAssociatedObject(self, allowRotationKey, @(allowRotation), OBJC_ASSOCIATION_COPY);
}

@end
