//
//  UIView+Constraints.m
//  PSUIKit
//
//  Created by Pavel Smelovsky on 02.04.15.
//  Copyright (c) 2015 Pavel Smelovsky. All rights reserved.
//

#import "UIView+Constraints.h"

@implementation UIView (Constraints)

- (NSLayoutConstraint *)widthConstraint
{
    return [self constraintWithFirstAttribute:NSLayoutAttributeWidth secondAttribute:NSLayoutAttributeNotAnAttribute secondItem:nil];
}

- (NSLayoutConstraint *)heightConstraint
{
    return [self constraintWithFirstAttribute:NSLayoutAttributeHeight secondAttribute:NSLayoutAttributeNotAnAttribute secondItem:nil];
}


- (void)setWidthConstraint:(CGFloat)constant
{
    [self setSizeConstraint:NSLayoutAttributeWidth constant:constant];
}

- (void)setHeightConstraint:(CGFloat)constant
{
    [self setSizeConstraint:NSLayoutAttributeHeight constant:constant];
}

- (void)setSizeConstraint:(NSLayoutAttribute)attribute constant:(CGFloat)constant
{
    NSLayoutConstraint* constraint = nil;
    if ((constraint = [self widthConstraint]) != nil) {
        constraint.constant = constant;
    }
    else {
        constraint = [NSLayoutConstraint constraintWithItem:self attribute:attribute relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:constant];
        [self addConstraint:constraint];
    }
    [self setNeedsLayout];
}


- (void)updateWidthConstraint:(CGFloat)constant
{
    NSLayoutConstraint* constraint = nil;
    if ( (constraint = [self widthConstraint]) ) {
        constraint.constant = constant;
    }
}

- (void)updateHeightConstraint:(CGFloat)constant
{
    NSLayoutConstraint* constraint = nil;
    if ( (constraint = [self heightConstraint]) ) {
        constraint.constant = constant;
    }
}



- (NSLayoutConstraint *)centerXConstraint
{
    if (self.superview != nil) {
        return [self centerXConstraintForView:self.superview];
    }
    return nil;
}

- (NSLayoutConstraint *)centerXConstraintForView:(UIView *)view
{
    return [self constraintWithFirstAttribute:NSLayoutAttributeCenterX secondAttribute:NSLayoutAttributeCenterX secondItem:view];
}

- (NSLayoutConstraint *)centerYConstraint
{
    if (self.superview != nil) {
        return [self centerYConstraintForView:self.superview];
    }
    return nil;
}

- (NSLayoutConstraint *)centerYConstraintForView:(UIView *)view
{
    return [self constraintWithFirstAttribute:NSLayoutAttributeCenterY secondAttribute:NSLayoutAttributeCenterY secondItem:view];
}

- (NSLayoutConstraint *)constraintWithFirstAttribute:(NSLayoutAttribute)firstAttribute secondAttribute:(NSLayoutAttribute)secondAttribute secondItem:(UIView *)secondItem
{
    NSLayoutConstraint* constraint = nil;

    for (NSLayoutConstraint* item in [self constraints])
    {
        if (item.firstAttribute == firstAttribute && item.secondAttribute == secondAttribute && item.secondItem == secondItem)
        {
            constraint = item;
            break;
        }
    }

    return constraint;
}



- (NSArray *)setCenterConstraintsToView:(UIView *)view
{
    NSLayoutConstraint* centerX = [self setCenterXConstraintToView:view constant:0];
    NSLayoutConstraint* centerY = [self setCenterYConstraintToView:view constant:0];
    return @[centerX, centerY];
}

- (NSLayoutConstraint *)setCenterXConstraintToView:(UIView *)view constant:(CGFloat)constant
{
    NSLayoutConstraint* constraint = nil;

    // check if centerX constraint exists - use it
    if ( (constraint = [self centerXConstraintForView:view]) )
    {
        constraint.constant = constant;
    }
    else // otherwise create new constraint and add it to view
    {
        constraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:constant];
        [view addConstraint:constraint];
    }
    return constraint;
}

- (NSLayoutConstraint *)setCenterYConstraintToView:(UIView *)view constant:(CGFloat)constant
{
    NSLayoutConstraint* constraint = nil;

    // check if centerY constraint exists - use it
    if ( (constraint = [self centerYConstraintForView:view]) )
    {
        constraint.constant = constant;
    }
    else // otherwise create new constraint and add it to view
    {
        constraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:constant];
        [view addConstraint:constraint];
    }
    return constraint;
}

@end
