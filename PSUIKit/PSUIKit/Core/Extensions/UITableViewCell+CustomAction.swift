//
//  UITableViewCell_CustomAction.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 13.05.16.
//  Copyright © 2016 Pavel Smelovsky. All rights reserved.
//

import UIKit

extension UITableViewCell {
    @IBAction func customAccessoryAction(_ sender: Any?) {
        guard let tableView = self.tableView else {
            preconditionFailure("This UITableViewCell doesn't use in UITableView")
        }

        guard let indexPath = tableView.indexPath(for: self) else {
            preconditionFailure("Cannot find indexPath of current cell")
        }

        guard let delegate = tableView.delegate else {
            preconditionFailure("Source tableView doesn't have delegate")
        }

        delegate.tableView?(tableView, accessoryButtonTappedForRowWith: indexPath)
    }
}
