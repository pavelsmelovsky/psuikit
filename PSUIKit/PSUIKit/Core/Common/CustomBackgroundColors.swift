//
//  CustomBackgroundColors.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 09.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import UIKit
import ObjectiveC

enum AdjustedColorState {
    case highlighted
    case disabled
}

public protocol CustomBackgroundColors: class {
    var currentColor: UIColor? { get }

    var defaultColor: UIColor? { get set }
    var highlightedColor: UIColor? { get set }
    var highlightedSelectedColor: UIColor? { get set }
    var selectedColor: UIColor? { get set }
    var disabledColor: UIColor? { get set }

    func color(didChange color: UIColor?, for state: UIControl.State)
}

private let kColorsProperty = malloc(4)

public extension CustomBackgroundColors where Self: UIControl {
    private var internalColors: [UInt: UIColor]! {
        get {
            return objc_getAssociatedObject(self, kColorsProperty!) as? [UInt: UIColor]
        }
        set {
            objc_setAssociatedObject(self, kColorsProperty!, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    var colors: [UInt: UIColor] {
        get {
            if internalColors == nil {
                internalColors = [:]
            }
            return internalColors
        }
        set {
            internalColors = newValue
        }
    }
}

public extension CustomBackgroundColors where Self: UIControl {
    func color(for state: UIControl.State) -> UIColor? {
        if let color = self.colors[state.rawValue] {
            return color
        }

        if state.contains(.highlighted) {
            let isSelected = state.contains(.selected)
            if isSelected, let color = self.color(for: .highlighted) {
                return color
            } else {
                let color = self.color(for: isSelected ? .selected : .normal)
                return color?.adjust(.darker)
            }
        } else if state.contains(.disabled) {
            return self.color(for: .normal)?.adjust(.lighter)
        } else if state.contains(.selected) {
            return self.color(for: .normal)
        } else {
            return nil
        }
    }

    func set(color: UIColor?, for state: UIControl.State) {
        if let color = color {
            colors[state.rawValue] = color
        } else {
            colors.removeValue(forKey: state.rawValue)
        }

        self.color(didChange: color, for: state)
    }
}

public extension CustomBackgroundColors where Self: UIControl {
    var currentColor: UIColor? {
        return self.color(for: self.state)
    }
}
