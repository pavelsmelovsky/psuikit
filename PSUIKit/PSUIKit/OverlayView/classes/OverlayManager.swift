//
//  OverlayManager.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 18.09.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import UIKit

public class OverlayManager {
    public var isOverlayVisible: Bool {
        return overlayContainer != nil
    }

    public private(set) weak var overlayContainer: OverlayContainer?
    public weak var overlayViewDelegate: OverlayViewDelegate?

    private unowned var overlayViewDependency: OverlayViewDependency

    private var resources: [String: OverlayResourceType] = [:]
    private var contentViewBoundsObservation: NSKeyValueObservation?

    private var overlayContentView: UIView {
        return overlayViewDependency.overlayContentView
    }

    internal init(overlayViewDependency: OverlayViewDependency) {
        self.overlayViewDependency = overlayViewDependency
    }

    public func register(nibName: String, forOverlayName overlayName: String) {
        let nib = UINib(nibName: nibName, bundle: nil)
        resources[overlayName] = .nib(nib: nib)
    }

    @discardableResult
    public func show(overlayNamed name: String, animated: Bool = true) -> Bool {
        let container = self.overlayContainer ?? self.instantiateContainerView(in: self.overlayContentView)
        self.overlayContainer = container

        do {
            // target view that will be appear
            let targetView = try self.loadOverlayView(named: name)
            container.targetOverlayView = targetView

            // current overlay view that presented (if available)
            let overlayView = container.overlayView

            // call delegate appearance methods
            self.overlayViewDelegate?.overlay?(willAppear: targetView)
            if let view = overlayView {
                self.overlayViewDelegate?.overlay?(willDisappear: view)
            }

            self.overlayContentView.bringSubviewToFront(container)

            container.commit(animated: animated) { [weak self, unowned targetView] (finished) in
                guard let sSelf = self else { return }

                if finished {
                    sSelf.overlayViewDelegate?.overlay?(didAppear: targetView)
                    if let view = overlayView {
                        sSelf.overlayViewDelegate?.overlay?(didDisappear: view)
                    }
                }
            }

            return true
        } catch let error {
            #if DEBUG
            fatalError("Error showing overlay view: \(error.localizedDescription)")
            #else
            return false
            #endif
        }
    }

    public func hide(animated: Bool) {
        self.contentViewBoundsObservation?.invalidate()
        self.contentViewBoundsObservation = nil

        if let scrollView = self.overlayContentView as? UIScrollView {
            scrollView.isScrollEnabled = true
        }

        if let view = self.overlayContainer?.overlayView {
            self.overlayViewDelegate?.overlay?(willDisappear: view)
        }

        guard let container = self.overlayContainer else { return }
        self.overlayContainer = nil

        container.hide(animated: animated) { [weak self, unowned container] in
            if let view = container.overlayView {
                self?.overlayViewDelegate?.overlay?(didDisappear: view)
            }

            container.removeFromSuperview()
        }
    }

    private func instantiateContainerView(in contentView: UIView) -> OverlayContainer {
        let container = OverlayContainer(frame: contentView.bounds)

        container.preservesSuperviewLayoutMargins = true
        container.translatesAutoresizingMaskIntoConstraints = true

        if let scrollView = contentView as? UIScrollView {
            scrollView.isScrollEnabled = false
        }

        contentView.addSubview(container)

        debugPrint("contentView.bounds: \(contentView.bounds), container.frame: \(container.frame)")

        self.contentViewBoundsObservation = contentView.observe(\.bounds, options: [.old, .new]) { (_, value) in
            if let newValue = value.newValue {
                container.frame = newValue
                debugPrint("contentView.bounds: \(contentView.bounds), container.frame: \(container.frame)")
            }
        }

        return container
    }

    private func loadOverlayView(named name: String) throws -> OverlayView {
        guard let resource = self.resources[name] else {
            throw OverlayError.notRegistered(message: "Resource with name \(name) is not registered")
        }

        let overlayView: OverlayView

        switch resource {
        case .nib(let nib):
            overlayView = try load(nib: nib, withIdentifier: name)
        default:
            throw OverlayError.invalidResource(message: "Invalid resource with name \(name)")
        }

        overlayView.identifier = name
        return overlayView
    }

    private func load(nib: UINib, withIdentifier identifier: String) throws -> OverlayView {
        let views = nib.instantiate(withOwner: nil, options: nil)

        guard let view = views.first as? OverlayView else {
            throw OverlayError.invalidResource(message: "First element of nib must be kind of OverlayView class")
        }

        return view
    }
}

private enum OverlayError: Error {
    case notRegistered(message: String)
    case invalidResource(message: String)
}

extension OverlayError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .notRegistered(let message):
            return message
        case .invalidResource(let message):
            return message
        }
    }
}

private enum OverlayResourceType {
    case nib(nib: UINib)
    case klass(klass: OverlayView.Type)
}
