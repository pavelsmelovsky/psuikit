//
//  WeakReference.swift
//  OverlayView
//
//  Created by Pavel Smelovsky on 18.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

class WeakReference<T> where T: AnyObject {
    weak var reference: T?

    init?(_ reference: T?) {
        guard let ref = reference else { return nil }
        self.reference = ref
    }
}
