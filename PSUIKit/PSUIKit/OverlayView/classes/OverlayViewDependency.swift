//
//  OverlayViewDependency.swift
//  OverlayView
//
//  Created by Pavel Smelovsky on 18.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import UIKit
import ObjectiveC

public protocol OverlayViewDependency: class {
    var overlayContentView: UIView { get }

    var isOverlayVisible: Bool { get }

    var overlayViewDelegate: OverlayViewDelegate? { get set }

    func register(nibName: String, forOverlayName overlayName: String)

    @discardableResult
    func show(overlayNamed name: String, animated: Bool) -> Bool

    func hide(animated: Bool)
}

public extension OverlayViewDependency {
    var isOverlayVisible: Bool {
        return self.overlayManager.isOverlayVisible
    }

    var overlayViewDelegate: OverlayViewDelegate? {
        set {
            self.overlayManager.overlayViewDelegate = newValue
        }
        get {
            return self.overlayManager.overlayViewDelegate
        }
    }

    func register(nibName: String, forOverlayName overlayName: String) {
        self.overlayManager.register(nibName: nibName, forOverlayName: overlayName)
    }

    @discardableResult
    func show(overlayNamed name: String, animated: Bool) -> Bool {
        return self.overlayManager.show(overlayNamed: name, animated: animated)
    }

    func hide(animated: Bool) {
        self.overlayManager.hide(animated: animated)
    }
}

// MARK: - Properties

private let kOverlayManagerProperty = malloc(4)!

public extension OverlayViewDependency {
    var overlayManager: OverlayManager {
        if let overlayManager = _overlayManager {
            return overlayManager
        }

        _overlayManager = OverlayManager(overlayViewDependency: self)
        return _overlayManager!
    }

    private var _overlayManager: OverlayManager? {
        get {
            return objc_getAssociatedObject(self, kOverlayManagerProperty) as? OverlayManager
        }
        set {
            objc_setAssociatedObject(self, kOverlayManagerProperty, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
