//
//  LoadingOverlayViewDependency.swift
//  OverlayView
//
//  Created by Pavel Smelovsky on 18.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import UIKit

public protocol LoadingOverlayViewDependency: OverlayViewDependency {
    typealias LoadingCallback = (_ isEmpty: Bool) -> Swift.Void

    var isLoading: Bool { get }

    var emptyViewOverlayName: String! { get set }
    var loadingViewOverlayName: String! { get set }

    func instantiateEmptyView()

    func performLoading(animated: Bool, _ block: (_ completion: @escaping LoadingCallback) -> Swift.Void)

    @discardableResult
    func showLoading(animated: Bool) -> Bool
    func hideLoading(animated: Bool, isEmpty: Bool)
}

public extension LoadingOverlayViewDependency {
    func instantiateEmptyView() {
        self.register(nibName: emptyViewOverlayName, forOverlayName: emptyViewOverlayName)
        self.register(nibName: loadingViewOverlayName, forOverlayName: loadingViewOverlayName)
    }

    func performLoading(animated: Bool, _ block: (_ completion: @escaping LoadingCallback) -> Swift.Void) {
        self.showLoading(animated: animated)

        block { [weak self] isEmpty in
            // always hide loading overlay animated
            self?.hideLoading(animated: true, isEmpty: isEmpty)
        }
    }

    @discardableResult
    func showLoading(animated: Bool) -> Bool {
        if !isLoading {
            self.show(overlayNamed: loadingViewOverlayName, animated: animated)
            isLoading = true
            return true
        }
        return false // if loading is currently active
    }

    /// Hide loading view.
    /// If isEmpty is true then show EmptyView
    func hideLoading(animated: Bool, isEmpty: Bool) {
        isLoading = false

        if isEmpty {
            self.show(overlayNamed: emptyViewOverlayName, animated: animated)
        } else {
            self.hide(animated: animated)
        }
    }
}

private let kIsLoadingProperty = malloc(4)!

public extension LoadingOverlayViewDependency {
    fileprivate(set) var isLoading: Bool {
        get {
            return (objc_getAssociatedObject(self, kIsLoadingProperty) as? Bool) ?? false
        }
        set {
            objc_setAssociatedObject(self, kIsLoadingProperty, newValue, .OBJC_ASSOCIATION_COPY_NONATOMIC)
        }
    }
}
