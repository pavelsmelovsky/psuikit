//
//  OverlayContainer.swift
//  OverlayView
//
//  Created by Pavel Smelovsky on 18.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import UIKit

public class OverlayContainer: UIView {
    override public var backgroundColor: UIColor? {
        get {
            return backgroundView?.backgroundColor
        }
        set {
            backgroundView?.backgroundColor = newValue
        }
    }

    public var animationDuration: TimeInterval = 0.25

    public private(set) weak var overlayView: OverlayView?

    internal weak var targetOverlayView: OverlayView? {
        didSet {
            if let view = targetOverlayView {
                self.set(target: view)
            }
        }
    }

    private var backgroundView: _OverlayBackgroundView! {
        didSet {
            if let backgroundView = backgroundView, backgroundView.superview == nil {
                self.addSubview(backgroundView)
            }

            if let oldView = oldValue {
                oldView.removeFromSuperview()
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        commonInit()
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("Decodable initializer is not supported")
    }

    private func commonInit() {
        self.isOpaque = false
        self.backgroundColor = .clear

        self.backgroundView = self.setupBackgroundView()
    }

    private func setupBackgroundView() -> _OverlayBackgroundView {
        let view = _OverlayBackgroundView(frame: self.bounds)

        view.translatesAutoresizingMaskIntoConstraints = true
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.backgroundColor = .white

        view.isOpaque = true
        view.alpha = 0

        return view
    }

    private func set(target: OverlayView) {
        target.container = self

        target.translatesAutoresizingMaskIntoConstraints = false
        target.isHidden = true
        target.alpha = 0

        self.addSubview(target)

        // configure constraints for target view
        self.adjust(overlayView: target)
    }

    internal func commit(animated: Bool, completion: @escaping (_ finished: Bool) -> Swift.Void) {
        // layout before any animations were performe
        self.layoutIfNeeded()

        let previousView = overlayView
        overlayView = targetOverlayView
        targetOverlayView = nil

        overlayView?.willAppear(animated: animated)
        previousView?.willDisappear(animated: animated)

        overlayView?.isHidden = false

        if animated {
            self.transition(fromView: previousView, toView: overlayView, backgroundView: self.backgroundView, completion: completion)
        } else {
            self.overlayView?.alpha = 1.0
            self.backgroundView?.alpha = 1.0

            previousView?.removeFromSuperview()
            previousView?.didDisappear(animated: false)

            overlayView?.didAppear(animated: false)
        }
    }

    private func transition(fromView: OverlayView?, toView: OverlayView?, backgroundView: UIView?, completion: @escaping (_ finished: Bool) -> Swift.Void) {
        let options: UIView.AnimationOptions = [.curveEaseOut, .allowAnimatedContent]
        UIView.animate(withDuration: animationDuration, delay: 0, options: options, animations: { [weak fromView, weak toView, weak backgroundView] in
            fromView?.alpha = 0
            toView?.alpha = 1
            backgroundView?.alpha = 1
        }, completion: { [weak fromView, weak toView] finished in
            debugPrint("Animation finished: \(finished)")

            fromView?.didDisappear(animated: true)
            fromView?.removeFromSuperview()

            toView?.didAppear(animated: true)
        })
    }

    internal func hide(animated: Bool, _ completion: @escaping () -> Swift.Void) {
        let block: (Bool) -> Swift.Void = { [weak self] finished in
//            guard finished else { return }
            guard let sSelf = self else { return }

            sSelf.overlayView?.didDisappear(animated: animated)
            sSelf.overlayView?.removeFromSuperview()

            completion()
        }

        if animated {
            let options: UIView.AnimationOptions = [.curveEaseInOut, .allowAnimatedContent]

            UIView.animate(withDuration: animationDuration, delay: 0, options: options, animations: { [weak self] () in
                self?.alpha = 0
                self?.backgroundView?.alpha = 0
            }, completion: block)
        } else {
            self.backgroundView?.alpha = 0
            block(true)
        }
    }

    private func adjust(overlayView: UIView) {
        guard let superview = overlayView.superview else { return }

        superview.addConstraints([
            overlayView.leadingAnchor.constraint(equalTo: superview.leadingAnchor),
            overlayView.topAnchor.constraint(equalTo: superview.topAnchor),
            superview.trailingAnchor.constraint(equalTo: overlayView.trailingAnchor),
            superview.bottomAnchor.constraint(equalTo: overlayView.bottomAnchor)
        ])
    }
}
