//
//  OverlayViewDelegate.swift
//  OverlayView
//
//  Created by Pavel Smelovsky on 19.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

@objc
public protocol OverlayViewDelegate: class {
    @objc optional func overlay(willAppear overlayView: OverlayView)
    @objc optional func overlay(didAppear overlayView: OverlayView)

    @objc optional func overlay(willDisappear overlayView: OverlayView)
    @objc optional func overlay(didDisappear overlayView: OverlayView)
}

internal class WeakOverlayViewDelegate {
    weak var delegate: OverlayViewDelegate?

    init?(_ delegate: OverlayViewDelegate?) {
        guard delegate != nil else { return nil }
        self.delegate = delegate
    }
}
