//
//  OverlayView.swift
//  OverlayView
//
//  Created by Pavel Smelovsky on 18.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import UIKit

open class OverlayView: UIView {
    public typealias OverlayViewCallback = (_ view: OverlayView) -> Swift.Void

    public internal(set) var container: OverlayContainer?
    public internal(set) var identifier: String!

    public private(set) weak var tapGesture: UITapGestureRecognizer? {
        didSet {
            if let gesture = tapGesture {
                self.addGestureRecognizer(gesture)
            } else if tapGesture == nil, let oldValue = oldValue {
                self.removeGestureRecognizer(oldValue)
            }
        }
    }

    public var tapCallback: OverlayViewCallback? {
        didSet {
            if tapCallback != nil {
                self.prepareTapGesture()
            } else {
                self.disableTapGesture()
            }
        }
    }

    // MARK: - Appearance

    open func willAppear(animated: Bool) {

    }

    open func didAppear(animated: Bool) {

    }

    open func willDisappear(animated: Bool) {

    }

    open func didDisappear(animated: Bool) {

    }

    // MARK: - Tap gesture

    private func prepareTapGesture() {
        if let gesture = self.tapGesture {
            gesture.isEnabled = true
        } else {
            self.tapGesture = instantiateTapGesture()
        }
    }

    private func instantiateTapGesture() -> UITapGestureRecognizer {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        return tapGesture
    }

    private func disableTapGesture() {
        self.tapGesture?.isEnabled = false
    }

    @objc private func handleTapGesture(_ gesture: UITapGestureRecognizer) {
        self.tapCallback?(self)
    }
}
