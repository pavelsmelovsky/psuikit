//
//  AppearanceAnimation.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 26.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import UIKit

private let kViewAnimation = "kViewAnimation"

public class FadeAnimation: NSObject, CAAnimationDelegate {
    public typealias Completion = () -> Swift.Void

    fileprivate unowned var view: UIView

    public var duration: TimeInterval = 0.25 {
        didSet {
            self.animation.duration = duration
        }
    }

    private var animation: CABasicAnimation!

    public var completion: Completion?

    deinit {
        self.view.layer.removeAnimation(forKey: kViewAnimation)
    }

    required public init(view: UIView, duration: TimeInterval) {
        self.view = view

        super.init()

        self.duration = duration

        self.animation = self.createAnimation()
    }

    private func createAnimation() -> CABasicAnimation {
        let animation = CABasicAnimation(keyPath: "opacity")
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.duration = duration
        animation.delegate = self
        return animation
    }

    fileprivate func animate(fromValue: Float, toValue: Float) {
        view.appearanceAnimation = self

        animation.fromValue = fromValue
        animation.toValue = toValue

        view.layer.add(animation, forKey: kViewAnimation)
    }

    // MARK: - Animation delegate

    public func animationDidStart(_ anim: CAAnimation) {
        //
    }

    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag {
            if let animation = anim as? CABasicAnimation,
                let value = animation.toValue as? Float {
                self.view.layer.opacity = value
                self.view.layer.removeAnimation(forKey: kViewAnimation)
            }

            self.completion?()
        } else {
            debugPrint()
        }
    }
}

public class FadeInAnimation: FadeAnimation {
    public func commit(completion: Completion?) {
        self.completion = completion

        let layer = self.view.layer.presentation()
        let fromValue = layer?.opacity ?? 0
        self.animate(fromValue: fromValue, toValue: 1.0)
    }
}

public class FadeOutAnimation: FadeAnimation {
    public var removeViewOnCompletion: Bool = true

    public func commit(completion: Completion?) {
        self.completion = completion

        let layer = self.view.layer.presentation()
        let fromValue = layer?.opacity ?? 1
        self.animate(fromValue: fromValue, toValue: 0.0)
    }

    public override func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        super.animationDidStop(anim, finished: flag)
    }
}

private let kAppearanceAnimationProperty = malloc(4)!

extension UIView {
    var appearanceAnimation: FadeAnimation? {
        get {
            return objc_getAssociatedObject(self, kAppearanceAnimationProperty) as? FadeAnimation
        }
        set {
            objc_setAssociatedObject(self, kAppearanceAnimationProperty, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
