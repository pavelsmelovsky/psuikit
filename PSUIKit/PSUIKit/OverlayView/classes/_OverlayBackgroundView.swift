//
//  _OverlayBackgroundView.swift
//  PSUIKit
//
//  Created by Pavel Smelovsky on 18.09.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import UIKit

// swiftlint:disable type_name
class _OverlayBackgroundView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)

        self.isOpaque = false
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
// swiftlint:enable type_name
