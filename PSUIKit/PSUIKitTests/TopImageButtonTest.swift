//
//  TopImageButtonTest.swift
//  PSUIKitTests
//
//  Created by Pavel Smelovsky on 10.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import XCTest

@available(iOS 9.0, *)
class TopImageButtonTest: XCTestCase {

    // swiftlint:disable line_length
    override func setUp() {
        super.setUp()

        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    // swiftlint:enable line_length

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

}
