//
//  CustomBackgroundColorsTest.swift
//  PSUIKitTests
//
//  Created by Pavel Smelovsky on 09.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import XCTest
import UIKit
@testable import PSUIKit

class TestView: UIControl, CustomBackgroundColors {
    var defaultColor: UIColor? {
        get {
            return self.color(for: .normal)
        }
        set {
            self.set(color: newValue, for: .normal)
        }
    }

    var highlightedColor: UIColor? {
        get {
            return self.color(for: .highlighted)
        }
        set {
            self.set(color: newValue, for: .highlighted)
        }
    }

    var highlightedSelectedColor: UIColor? {
        get {
            return self.color(for: [.highlighted, .selected])
        }
        set {
            self.set(color: newValue, for: [.highlighted, .selected])
        }
    }

    var selectedColor: UIColor? {
        get {
            return self.color(for: .selected)
        }
        set {
            self.set(color: newValue, for: .selected)
        }
    }

    var disabledColor: UIColor? {
        get {
            return self.color(for: .disabled)
        }
        set {
            self.set(color: newValue, for: .disabled)
        }
    }

    func color(didChange color: UIColor?, for state: UIControl.State) {
        //
    }
}

class CustomBackgroundColorsTest: XCTestCase {
    var testView: TestView!

    override func setUp() {
        super.setUp()
        self.testView = TestView()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testCustomBackgroundColors() {
        XCTAssert(self.testView.colors.isEmpty)

        let normalColor = UIColor.white
        let highlightedColor = UIColor.lightGray
        let highlightedSelectedColor = UIColor.darkGray
        let selectedColor = UIColor.gray
        let disabledColor = UIColor.black

        self.testView.defaultColor = normalColor
        self.testView.highlightedColor = highlightedColor
        self.testView.highlightedSelectedColor = highlightedSelectedColor
        self.testView.selectedColor = selectedColor
        self.testView.disabledColor = disabledColor

        XCTAssertTrue(self.testView.colors.count == 5)

        XCTAssertEqual(self.testView.colors[UIControl.State.normal.rawValue], normalColor)
        XCTAssertEqual(self.testView.colors[UIControl.State.highlighted.rawValue], highlightedColor)

        let highlightedSelected: UIControl.State = [.selected, .highlighted]
        XCTAssertEqual(self.testView.colors[highlightedSelected.rawValue], highlightedSelectedColor)
        XCTAssertEqual(self.testView.colors[UIControl.State.selected.rawValue], selectedColor)
        XCTAssertEqual(self.testView.colors[UIControl.State.disabled.rawValue], disabledColor)
    }

//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
}
