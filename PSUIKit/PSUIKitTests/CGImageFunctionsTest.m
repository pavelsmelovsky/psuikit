//
//  CGImageFunctionsTest.m
//  PSUIKitTests
//
//  Created by Pavel Smelovsky on 09.05.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

#import <XCTest/XCTest.h>
@import PSUIKit;

@interface CGImageFunctionsTest : XCTestCase

@property (nonatomic, weak) NSBundle *bundle;

@end

@implementation CGImageFunctionsTest

- (void)setUp {
    [super setUp];

    self.bundle = [NSBundle bundleForClass:self.class];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testAspectFit {
    // 500 × 764 pixels
    UIImage *image = [UIImage imageNamed:@"code" inBundle:self.bundle compatibleWithTraitCollection:nil];
    XCTAssertNotNil(image);

    CGRect rect = CGRectMake(0, 0, 250, 382);
    CGRect imageRect = CGAspectFitImageSize(image.CGImage, rect);
    NSLog(@"imageRect: %@", NSStringFromCGRect(imageRect));
    XCTAssertTrue(CGRectEqualToRect(rect, imageRect));

    CGRect rect2 = CGRectMake(0, 0, 300, 573);
    CGRect imageRect2 = CGAspectFitImageSize(image.CGImage, rect2);
    NSLog(@"imageRect2: %@", NSStringFromCGRect(imageRect2));
    // width 0.6, height 0.75
    XCTAssertTrue(CGRectGetWidth(imageRect2) == 300);
    XCTAssertTrue(CGRectGetHeight(imageRect2) == 458.4);
    XCTAssertTrue(CGRectGetMinX(imageRect2) == 0);
    XCTAssertTrue(CGRectGetMinY(imageRect2) == 57.3);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
