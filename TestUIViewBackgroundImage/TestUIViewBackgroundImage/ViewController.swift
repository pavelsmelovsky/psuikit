//
//  ViewController.swift
//  TestUIViewBackgroundImage
//
//  Created by Pavel Smelovsky on 09.05.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import UIKit
import PSUIKit

class ViewController: UIViewController {
    @IBOutlet weak var contentView: UIView!

    @IBOutlet weak var button: Button!

    var programButton: Button!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundImage = #imageLiteral(resourceName: "test-image-big")
        self.view.imageMode = .fill
        
        self.contentView.backgroundImage = #imageLiteral(resourceName: "test-image-small")
        self.contentView.imageMode = .aspectFit
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func onButton(_ button: Button) {
        button.isSelected = !button.isSelected

        if button.isSelected {
//            button.borderWidth = 2
            button.set(borderWidth: 5, animated: true)
        } else {
//            button.borderWidth = 0
            button.set(borderWidth: 0, animated: true)
        }
    }
}

