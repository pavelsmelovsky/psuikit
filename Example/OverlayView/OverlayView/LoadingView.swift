//
//  LoadingView.swift
//  OverlayView
//
//  Created by Pavel Smelovsky on 18.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import UIKit
import PSUIKit

class LoadingView: OverlayView {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var descriptionLabel: UILabel!

    deinit {
        debugPrint("Deinit LoadingView")
    }
}
