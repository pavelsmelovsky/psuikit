//
//  EmptyView.swift
//  OverlayView
//
//  Created by Pavel Smelovsky on 18.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import UIKit
import PSUIKit

class EmptyView: OverlayView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    deinit {
        debugPrint("Deinit EmptyView")
    }
}
