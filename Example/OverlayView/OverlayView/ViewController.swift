//
//  ViewController.swift
//  OverlayView
//
//  Created by Pavel Smelovsky on 18.11.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import UIKit
import PSUIKit

class ViewController: UIViewController, LoadingOverlayViewDependency, OverlayViewDelegate {
    func overlay(willAppear overlayView: OverlayView) {
        if let loadingView = overlayView as? LoadingView {
            loadingView.activityIndicator.startAnimating()
        } else if let emptyView = overlayView as? EmptyView {
            emptyView.tapCallback = { view in
                self.onButton(nil)
            }
        }

        overlayView.container?.backgroundColor = UIColor.blue
    }

    func overlay(didAppear overlayView: OverlayView) {
        //
    }

    func overlay(willDisappear overlayView: OverlayView) {
        //
    }

    var emptyViewOverlayName: String! = "EmptyView"
    var loadingViewOverlayName: String! = "LoadingView"

    var overlayContentView: UIView {
        return self.view
    }

    @IBOutlet weak var button: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.instantiateEmptyView()
        self.overlayViewDelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var times: [Int] = [500, 1000, 2000, 3000, 4000]
    @IBAction func onButton(_ sender: Any?) {
        self.showLoading(animated: true)
        var hide = true
        for time in times {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(time), execute: {
                if self.isLoading {
                    self.hideLoading(animated: true, isEmpty: true)
                } else {
                    self.showLoading(animated: true)
                }
            })

            hide = !hide
        }
    }

    @IBAction func onBarButton(_ sender: Any?) {
        if self.isLoading {
            self.hideLoading(animated: true, isEmpty: true)
        } else {
            self.showLoading(animated: true)
        }
    }
}
