//
//  TableViewController.swift
//  OverlayView
//
//  Created by Pavel Smelovsky on 18.09.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import UIKit
import PSUIKit

class TableViewController: UITableViewController, LoadingOverlayViewDependency {
    var overlayContentView: UIView {
        return self.tableView//.superview!
    }

    var emptyViewOverlayName: String! = "EmptyView"

    var loadingViewOverlayName: String! = "LoadingView"


    override func viewDidLoad() {
        super.viewDidLoad()

        self.instantiateEmptyView()
    }

    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)

        self.showLoading(animated: false)
    }

    private var count: Int = 0

    @IBAction func switchItems(_ sender: Any) {
        count = count == 0 ? 10 : 0

        self.tableView.reloadSections(IndexSet(integer: 0), with: .automatic)

        if count == 0 {
            self.hideLoading(animated: true, isEmpty: true)
        } else {
            self.hideLoading(animated: true, isEmpty: false)
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        cell.textLabel?.text = "Cell \(indexPath.row) title"

        return cell
    }
}
